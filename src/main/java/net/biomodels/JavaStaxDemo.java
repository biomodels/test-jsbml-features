package net.biomodels;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

public class JavaStaxDemo {
    public String getSBMLLevelAndVersion(String sbmlFilePath) throws IOException, XMLStreamException {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = xmlInputFactory.createXMLEventReader(Files.newInputStream(Paths.get(sbmlFilePath)));
        String strLevelVersion = "";
        while (reader.hasNext()) {
            XMLEvent nextEvent = reader.nextEvent();
            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();
                if (startElement.getName().getLocalPart() == "sbml") {
                    Iterator<Attribute> itAttrs = startElement.getAttributes();
                    String level = "";
                    String version = "";
                    while (itAttrs.hasNext()) {
                        Attribute attr = itAttrs.next();
                        if (attr.getName().getLocalPart() == "level") {
                            level = attr.getValue();
                        }
                        if (attr.getName().getLocalPart() == "version") {
                            version = attr.getValue();
                        }
                        strLevelVersion = "L".concat(level).concat("V").concat(version);
                    }
                    break;
                }
            }
        }
        return strLevelVersion;
    }

    public void run(final String sbmlFilePath) throws XMLStreamException, IOException {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(classLoader.getResource(sbmlFilePath).getFile());
        String absPath = file.getAbsolutePath();
        String strLevelVersion = getSBMLLevelAndVersion(absPath);
        System.out.println(strLevelVersion);
    }
}
