package net.biomodels;

import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class SbmlModelMetricsService {

    public static ModelStats getStatsFor(String modelPath) {
        SBMLDocument sbmlDocument;
        try {
            sbmlDocument = SbmlReaderService.parse(modelPath);
        } catch (Exception e) {
            System.err.println(e.toString());
            return null;
        }
        Path modelFilePath = Paths.get(modelPath).getFileName();
        if (null == modelFilePath) {
            System.err.printf("Non-existent model file %s.%n", modelPath);
            return null;
        }
        String fileName = modelFilePath.toString();
        return populateModelStats(sbmlDocument, new ModelStats(fileName));
    }

    private static ModelStats populateModelStats(SBMLDocument document, ModelStats stats) {
        //Map<String, SBasePlugin> packages = document.getExtensionPackages();
        Model model = Objects.requireNonNull(document).getModel();

        if (null == model) throw new IllegalStateException("Could not parse model " + document.getId());

        int mathElements = model.getMathContainerCount();
        int reactions = model.getReactionCount();
        int species = model.getNumSpecies();
        int parameters = model.getNumParameters();
        int localParameters = model.getNumLocalParameters();
        int compartments = model.getCompartmentCount();
        String qualPackage = model.getPackageName();
        String packageVersion = String.format("%s", model.getPackageVersion());

        Objects.requireNonNull(stats).setMathElements(mathElements);
        stats.setReactions(reactions);
        stats.setParameters(parameters);
        stats.setSpecies(species);
        stats.setLocalParameters(localParameters);
        stats.setCompartments(compartments);
        stats.setQualPackage(qualPackage + " " + packageVersion);
        return stats;
    }
}
