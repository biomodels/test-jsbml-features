package net.biomodels;

import com.thoughtworks.xstream.converters.ConversionException;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class SbmlReaderService {
    public static final String QUOTE = "\"";

    public static SBMLDocument parse(String modelPath) {
        try {
            if (null == modelPath || modelPath.trim().isEmpty())
                throw new IllegalArgumentException(modelPath);
            if (modelPath.startsWith(QUOTE) && modelPath.endsWith(QUOTE)) {
                modelPath = modelPath.substring(1, modelPath.length() - 1);
            }
            return new SBMLReader().readSBML(modelPath);
        } catch (XMLStreamException | IOException e) {
            String msg = String.format("Cannot read SBML model %s: %s", modelPath, e.toString());
            throw new IllegalStateException(msg);
        }
    }

    public static boolean validateSBMLFile(final String filePath) {
        boolean result = false;
        try {
            SBMLDocument doc = SbmlReaderService.parse(filePath);
            result = SbmlReaderService.validateSBMLDocument(doc);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean validateSBMLDocument(final SBMLDocument doc) {
        boolean result = false;
        try {
            int CONSISTENCY_ERRORS = doc.checkConsistency();
            if (CONSISTENCY_ERRORS == -1) {
                System.out.println("Cannot reach the online validator. Trying to fall back to the offline validator");
                CONSISTENCY_ERRORS = doc.checkConsistencyOffline();
            }
            return CONSISTENCY_ERRORS == 0;
        } catch (ConversionException e) {
            System.out.println(e.getMessage());
            result = false;
        }
        return result;
    }
}
