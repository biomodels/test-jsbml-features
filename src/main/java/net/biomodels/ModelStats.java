package net.biomodels;

import java.io.BufferedWriter;
import java.io.IOException;

public final class ModelStats {
    private int mathElements;
    private int reactions;
    private int species;
    private int parameters;
    private int localParameters;
    private int compartments;

    public String getQualPackage() {
        return qualPackage;
    }

    public void setQualPackage(String qualPackage) {
        this.qualPackage = qualPackage;
    }

    private String qualPackage;

    private String model;

    ModelStats(String model) {
        this.model = model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getMathElements() {
        return mathElements;
    }

    public int getReactions() {
        return reactions;
    }

    public String getModel() {
        return model;
    }

    public void setMathElements(int mathElements) {
        this.mathElements = mathElements;
    }

    public void setReactions(int reactions) {
        this.reactions = reactions;
    }

    public int getSpecies() {
        return species;
    }

    public void setSpecies(int species) {
        this.species = species;
    }

    public int getParameters() {
        return parameters;
    }

    public void setParameters(int parameters) {
        this.parameters = parameters;
    }

    public int getTotal() {
        return reactions + mathElements;
    }

    public void write(BufferedWriter writer) {
        String line = String.format("%s%s%s, %d, %d, %d, %d, %d, %d",
            SbmlReaderService.QUOTE, model, SbmlReaderService.QUOTE, reactions,
            mathElements, species, parameters, localParameters, compartments);
        try {
            writer.write(line);
            writer.newLine();
        } catch (IOException e) {
            System.err.printf("Could not write line %s.%n", line);
        }
    }

    public int getLocalParameters() {
        return localParameters;
    }

    public void setLocalParameters(int localParameters) {
        this.localParameters = localParameters;
    }

    public int getCompartments() {
        return compartments;
    }

    public void setCompartments(int compartments) {
        this.compartments = compartments;
    }
}
