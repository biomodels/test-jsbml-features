package net.biomodels;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Set;

public class SimpleCsvFileWriter {
    static void write(String location, Set<ModelStats> stats)
            throws IOException {
        Path out = Paths.get(Objects.requireNonNull(location));
        try (BufferedWriter writer = Files.newBufferedWriter(out, StandardCharsets.UTF_8)) {
            stats.forEach(s -> s.write(writer));
        }
    }
}
