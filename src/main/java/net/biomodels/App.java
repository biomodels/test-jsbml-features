package net.biomodels;

import org.sbml.jsbml.*;

import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class App {
    public static final int NO_ARG_CODE = 1;
    public static final int NOT_A_LOCATION_CODE = 2;

    public static void main(String[] args) throws IOException {
        // App.computeStatistics(args);
        // App.testProxy();
        // App.validateSBMLFile();
        // App.testJavaStaxDemo();
        // App.checkAnnotations();
        App.saveModelNameInSbmlL3V2();
    }

    static void saveModelNameInSbmlL3V2() {
        String filePath = "data/MODEL2403080002_cifelli08.xml";
        String newFilePath = "src/main/resources/data/MODEL2403080002_cifelli08_updated.xml";
        try (InputStream inputStream = App.class.getClassLoader().getResourceAsStream(filePath)) {
            SBMLReader sbmlReader = new SBMLReader();
            SBMLDocument doc = sbmlReader.readSBMLFromStream(inputStream);
            Model model = doc.getModel();
            String newName = "Cifelli2008 - whole-body vitamin A metabolism in humans";
            model.setName(newName);
            SBMLWriter sbmlWriter = new SBMLWriter();
            File outFile = new File(newFilePath); //getFileFromResource(newFilePath);
            sbmlWriter.writeSBML(doc, outFile);
        } catch (IOException | XMLStreamException e) {
            throw new RuntimeException(e);
        }
    }

    public static void checkAnnotations() {
        // the stream holding the file content

        String fileName = "data/BIOMD0000000688.xml";

        try (InputStream inputStream = App.class.getClassLoader().getResourceAsStream(fileName)) {
            SBMLReader sbmlReader = new SBMLReader();
            SBMLDocument doc = sbmlReader.readSBMLFromStream(inputStream);
            boolean valid = SbmlReaderService.validateSBMLDocument(doc);
            System.out.println(valid ? "Model is valid" : "Model is invalid");

            Model model = doc.getModel();
            CVTerm.Qualifier qualifier = CVTerm.Qualifier.BQM_IS;
            List<CVTerm> cVTerms = model.filterCVTerms(qualifier);
            for (CVTerm cvTerm : cVTerms) {
                System.out.println(cvTerm.getResources());
            }

        } catch (XMLStreamException | IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void testJavaStaxDemo() throws RuntimeException {
        try {
            String filePath = "data/00001-sbml-l2v1.xml";
            filePath = "data/HTimmR.xml";
            new JavaStaxDemo().run(filePath);
        } catch (XMLStreamException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void testProxy() throws IOException {
        URL u = new URL("https://www.google.co.uk");
        try (InputStream inputStream = u.openStream();
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             BufferedReader in = new BufferedReader(inputStreamReader)) {
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                System.out.println(inputLine);
        }
    }

    public static void validateSBMLFile() throws IOException {
        // the stream holding the file content
        System.out.println("Validating SBML files using JSBML with Online Validator");

        String fileName = "data/CcanadensisModel.xml";
        if (fileName.isEmpty()) {
            throw new IOException();
        } else {
            try (InputStream inputStream = App.class.getClassLoader().getResourceAsStream(fileName)) {
                SBMLReader sbmlReader = new SBMLReader();
                SBMLDocument doc = sbmlReader.readSBMLFromStream(inputStream);
                boolean valid = SbmlReaderService.validateSBMLDocument(doc);
                System.out.println(valid ? "Model is valid" : "Model is invalid");
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
        }
    }

    public static void computeStatistics(String[] args) throws IOException {
        if (args.length < 2) {
            String msg = String.format(
                "Missing command file arguments [INPUT_FILE] [OUTPUT_FILE]%n" +
                "where%n" +
                "\tINPUT_FILE is a file containing the models to be processed (1 model per line)%n" +
                "\tOUTPUT_FILE is the location of the file where the stats should be written");
            exitWithError(msg, NO_ARG_CODE);
        }

        String modelQueueFileArg = args[0];
        Path modelQueuePath = Paths.get(modelQueueFileArg);
        if (!Files.isRegularFile(modelQueuePath)) {
            String msg = String.format("Location '%s' is not a file.%n", modelQueueFileArg);
            exitWithError(msg, NOT_A_LOCATION_CODE);
        }

        String outputFileArg = args[1];
        Path outputFile = Paths.get(outputFileArg);
        Path parentFolder = outputFile.normalize().getParent();
        if (null == parentFolder ||
                !(Files.isDirectory(parentFolder) && Files.isWritable(parentFolder))) {
            String msg = String.format("Location '%s' is not writable%n", parentFolder);
            exitWithError(msg, NOT_A_LOCATION_CODE);
        }

        List<String> modelQueue = Files.readAllLines(modelQueuePath, StandardCharsets.UTF_8);
        Set<ModelStats> modelStats = getModelStats(modelQueue);
        SimpleCsvFileWriter.write(outputFileArg, modelStats);
    }

    private static Set<ModelStats> getModelStats(List<String> modelQueue) {
        return modelQueue.parallelStream()
            .map(SbmlModelMetricsService::getStatsFor)
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());
    }

    private static void exitWithError(String message, int statusCode) {
        System.err.println(message);
        System.exit(statusCode);
    }
}
